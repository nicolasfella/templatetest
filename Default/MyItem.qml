import org.kde.myt.templates as T

T.MyItem {

    property int val: 500

    color: val > 300 ? "blue" : "red"
}
