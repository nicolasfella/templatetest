import QtQuick
import org.kde.myt as MyT

Window {
    visible: true

    MyT.MyItem {
        width: 100
        height: width * 0.8
    }
}
