#include <QGuiApplication>

#include <QQmlApplicationEngine>

int main(int argc, char** argv) {

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.load(QUrl("qrc:/qt/qml/org/kde/templatetest/Main.qml"));

    return app.exec();
}
