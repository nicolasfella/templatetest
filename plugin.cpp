#include <QQmlEngineExtensionPlugin>
#include <QQmlEngine>

class MyTPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri) override
    {
        const auto style = qgetenv("MYT_STYLE");

        if (!style.isEmpty()) {
            qmlRegisterModuleImport("org.kde.myt", QQmlModuleImportAuto, "org.kde.myt." + style, QQmlModuleImportAuto);
        } else {
            qmlRegisterModuleImport("org.kde.myt", QQmlModuleImportAuto, "org.kde.myt.Default", QQmlModuleImportAuto);
        }
    }
};

#include "plugin.moc"
